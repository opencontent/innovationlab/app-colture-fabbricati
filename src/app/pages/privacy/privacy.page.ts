import { Component, OnInit } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.page.html',
  styleUrls: ['./privacy.page.scss'],
})
export class PrivacyPage implements OnInit {
  helpUrl: any;
  constructor( public sanitizer: DomSanitizer) {}

  ngOnInit() {
    this.helpUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
      'https://www.iubenda.com/privacy-policy/52326929'
    );
  }
}
