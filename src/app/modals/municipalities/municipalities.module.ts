import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MunicipalitiesPageRoutingModule } from './municipalities-routing.module';

import { MunicipalitiesPage } from './municipalities.page';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, MunicipalitiesPageRoutingModule],
  declarations: [MunicipalitiesPage],
})
export class MunicipalitiesPageModule {}
